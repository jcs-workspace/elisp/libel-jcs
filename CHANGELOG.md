

2018-12-03
* Remove useless function from word module. (libel-jcs)

2018-12-01
* Update word module. (libel-jcs)

2018-09-03
* Add base nav functions. (libel-jcs)
* Modefied first forward/backward character  and add first forward/backward 
 character to limit of whole buffer. (libel-jcs)

2018-09-01
* First setup project libel-jcs. (libel-jcs)
* libel-jcs-list implemented. (libel-jcs)
* libel-jcs-string implemented. (libel-jcs)
* libel-jcs-nav implemented. (libel-jcs)
* libel-jcs-comment implemented. (libel-jcs)
* libel-jcs-file implemented. (libel-jcs)
* libel-jcs-io implemented. (libel-jcs)
* libel-jcs-mode implemented. (libel-jcs)
* libel-jcs-face implemented. (libel-jcs)
* libel-jcs-font implemented. (libel-jcs)
* libel-jcs-char implemented. (libel-jcs)
* libel-jcs-word implemented. (libel-jcs)
* libel-jcs-line implemented. (libel-jcs)
